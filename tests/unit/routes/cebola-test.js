import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | cebola', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:cebola');
    assert.ok(route);
  });
});
