import Controller from '@ember/controller';
import {alias} from '@ember/object/computed';
export default Controller.extend({
    task: alias('model'),

    actions: {
        saveEdit(model, description, deadline){
            console.log(model.description);
            model.setProperties({description, deadline});
            model.save().then(() => this.transitionToRoute('task'));
        }
    }
});
