import Controller from '@ember/controller';

export default Controller.extend({
    tasks: ['a'],

    actions: {
            add(value){
                let arr = this.get('tasks');
                arr.push(value);
                this.set('tasks', []);
                this.set('tasks', arr);
            },

            exclude(value){
                let arr = this.get('tasks');
                let pos = arr.indexOf(value);
                arr.splice(pos,1);
                this.set('tasks', []);
                this.set('tasks', arr);    
            },

            edit(task){
                this.transitionToRoute('task.edit', task.get('id'));
            }
    }
});
